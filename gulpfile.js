'use strict'

const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const uglifycss = require('gulp-uglifycss');
const cssmin = require('gulp-cssmin');
const runSequence = require('run-sequence');

gulp.task('indexCopy', () => {
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('img', () => {
    gulp.src('./src/images/**/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: true }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./build/images'));
});

gulp.task('clean', () => {
    gulp.src('build', {read: false})
        .pipe(clean());
});

gulp.task('sass', () => {
    gulp.src('./src/scss/**/*.scss')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('./build/css'));
    gulp.src('./src/scss/reset.css')
        .pipe(rename({suffix: '.min'}))
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('dev', ['sass', 'uglify', 'indexCopy'], () => {
    browserSync.init({
        server: './build/'
    });
    gulp.watch('./src/js/**/*.js', ['uglify']);
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./src/index.html', ['indexCopy']);
    gulp.watch('./src/index.html').on('change', browserSync.reload);
});

gulp.task('build',  function () {
    runSequence('clean', 'sass', 'uglify', 'img', 'create-fonts-files', 'copy-jquery-files', 'indexCopy', function () {});
});

gulp.task('copy-jquery-files', () => {
    gulp.src('./src/jquery/**/*')
        .pipe(gulp.dest('./build/jquery'));
});

gulp.task('create-fonts-files', () => {
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./build/fonts'));
});

gulp.task('uglify', () => {
    gulp.src('./src/js/*.js')
        .pipe(concat('bundle.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./build/js'));
});

gulp.task('default', ['dev']);
