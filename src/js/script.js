/* -------slider------ */
$(document).ready(function(){
  $('.carousel__container__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.carousel__container_arrow-prev'),
    nextArrow: $('.carousel__container_arrow-next'),
    fade: true,
  });
});
/* -------burger------ */
$(document).ready(function(){
  $(".header__burger").click(function(){
    $(".header__menu").show();
    $(".header__burger").hide();
    $(".header__cross").show();
  });
});
$(document).ready(function(){
  $(".header__cross").click(function(){
    $(".header__menu").hide();
    $(".header__burger").show();
    $(".header__cross").hide();
  });
});
